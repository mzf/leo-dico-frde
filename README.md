Leo Dico French-German search plugin for firefox
================================================

This is a search plugin for firefox that enable automatic search 
in the online french-german dictionary service of [leo.org](http://dict.leo.org/frde).

Installation
------------
Visit the [install page](http://moleculext.github.com/leo-dico-frde/) or run the install.html file in your browser, a new window must pop up asking for installation.

Successfully tested with firefox 14.

Remark
------

I am not affiliated directly or indirectly with the [leo.org](http://www.leo.org) website.
I just noticed that google traduction service was really too light for professionnal use,
so I switched to a better one :-)
